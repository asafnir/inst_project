/**
*  The main application module
*/

app = angular.module('linkedinApp', []);

app.controller('HomeController', function ($scope){
	$scope.formShow = true;
	$scope.submit = function() {
		if ($scope.email) {
			$scope.formShow = false;
		}
	}
})

app.controller('DashboardController', function ($scope) {
});

app.controller('SignupController', function ($scope) {
});

app.controller('CampaignsController', function ($scope, $http) {
	$scope.campaigns = [];

	$scope.getData = function() {
		$http.get("/campaigns").then(function(res){
			$scope.campaigns = res.data;
			console.log(res.data);
		});
	};

	$scope.getData();
})

app.controller('NewCampaignController', function ($scope) {
	
});

app.controller('UserInformationController', function ($scope) {

});
