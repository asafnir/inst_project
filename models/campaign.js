'use strict';
module.exports = function(sequelize, DataTypes) {
  var Campaign = sequelize.define('Campaign', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    UserId: DataTypes.INTEGER,
    invite_message: DataTypes.TEXT,
    search_filters: DataTypes.TEXT,
    exclude_companies: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        Campaign.belongsTo(models.User);
      }
    }
  });
  return Campaign;
};

